<?php

class CountryController extends AdminController {

    /**
     * Demo Repository
     *
     * @var Demo
     */
    public function __construct() {
        parent::__construct();
        $this->view_path = "letstalk::admin.country";
        View::share('project_name',Config::get('setup.project_name'));
        View::share('theme',Config::get('theme.theme'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $filters = Input::get("filter", array());
        $countrys = Country::where("deleted", "0");
        if (!empty($filters) && is_array($filters)) {
            foreach ($filters as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = 'like';
                    $countrys->where($column, $operator, $row["value"]);
                }
            }
        }
        $countrys = $countrys->paginate(Config::get("bbcspl.par_page", 10));
        return View::make("{$this->view_path}.index", compact('countrys'));
    }

    public function create()
    {
        return View::make("{$this->view_path}.create");
    }

    public function store()
    {   
        $input = Input::except('_token');
        Validator::extend('validate_field', 'countryvalidator@validate_field');
        $validation = countryvalidator::create();
        if ($validation->passes()) {
           
            Country::create($input);
            return Redirect::route('admin.country.create')
                                ->with('success', 'Country Added Successfully.');
        }
        return Redirect::route('admin.country.create')
                        ->withInput()
                        ->withErrors($validation->errors);
    }

    public function edit($id)
    {
        $country = Country::find($id);
        if (is_null($country)) {
            return Redirect::route('admin.country.index');
        }
        return View::make("{$this->view_path}.edit", compact('country'));
    }

    public function update($id)
    {   
        $input = Input::except('_method','_token');
        Validator::extend('validate_field', 'countryvalidator@validate_field');
        $validation = countryvalidator::create();
        if ($validation->passes()) {

            if (Country::find($id)->update($input)) {
                return Redirect::route('admin.country.index')
                                ->with('success', 'Country Updated successfully.');
            } else {
                return Redirect::route('admin.country.index')
                                ->with('error', 'Country updating error.');
            }
        }
        return Redirect::route('admin.country.edit', $id)
                        ->withInput()
                        ->withErrors($validation->errors);
    }

    public function destroy($id) {
        if (Country::find($id)->update(array("deleted" => "1"))) {
            return Redirect::route('admin.country.index')
                            ->with('success', 'Country deleted successfully.');
        } else {
            return Redirect::route('admin.country.index')
                            ->with('error', 'Country deleting error.');
        }
    }
}