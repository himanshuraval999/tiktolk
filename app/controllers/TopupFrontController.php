<?php

class TopupFrontController extends BaseController {

    /**
     * Demo Repository
     *
     * @var Demo
     */

    public function __construct() {
        parent::__construct();
      
        View::share('project_name',Config::get('setup.project_name'));
        View::share('front_theme',Config::get('theme.fronttheme'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $countrys = Country::where('deleted','=',0)->groupby('country_name')->get()->toArray();
        Return View::make("frontend.topup.index",compact('countrys'));
    }

    public function get_rates()
    {       
            $from = $_GET['from'];
            $to = $_GET['to'];
            $rates = Rates::where('from','=',$from)->where('to','=',$to)->where('deleted','=','0')->get()->toArray();
            $rates = json_encode($rates[0]);
            print_r($rates);
            
    }
}