<?php

class RatesController extends AdminController {

    /**
     * Demo Repository
     *
     * @var Demo
     */
    public function __construct() {
        parent::__construct();
        $this->view_path = "letstalk::admin.rates";
        View::share('project_name',Config::get('setup.project_name'));
        View::share('theme',Config::get('theme.theme'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $filters = Input::get("filter", array());
        $rates = Rates::where("deleted", "0");
        $countries = Country::where('deleted','=',0)->groupby('country_name')->get()->toArray();
        if (!empty($filters) && is_array($filters)) {
            foreach ($filters as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = 'like';
                    $rates->where($column, $operator, $row["value"]);
                }
            }
        }
        $rates = $rates->paginate(Config::get("bbcspl.par_page", 10));
        return View::make("{$this->view_path}.index", compact('rates','countries'));
    }

    public function create()
    {
         $countries = Country::where('deleted','=',0)->groupby('country_name')->get()->toArray();
        return View::make("{$this->view_path}.create", compact('countries'));
    }

    public function store()
    {   
        $input = Input::except('_token','company','rates_min','minutes_for_5');
        
        $minutes_for_5 = Input::get('minutes_for_5');
        $company=  Input::get('company');
        $rates_min = Input::get('rates_min');
       
        Validator::extend('validate_field', 'ratesvalidator@validate_field');
        $validation = ratesvalidator::create();
        if ($validation->passes()) {
           
            $id = Rates::create($input)->getkey();
            if(!empty($rates_min)){
                $rates_min = (array)$rates_min;

             foreach ( $rates_min as $key => $value) {
                    $items[] = array(
                                'rates_id'=>$id,
                                'rates_min'=>$value,
                                'minutes_for_5'=> $minutes_for_5[$key],
                                'company'=>$company[$key],
                            );
                }
            }
            DB::table('rates_by_company')->insert($items);
            print_r($items);
            return Redirect::route('admin.rates.index')
                                ->with('success', 'Rate Added Successfully.');
        }
        
        return Redirect::route('admin.rates.create')
                        ->withInput()
                        ->withErrors($validation->errors);
    }

    public function edit($id)
    {
        $rate = Rates::find($id);
        $rates_by_company  = DB::table('rates_by_company')->where('rates_id','=',$id)->where('deleted','=',0)->get();
        $countries = Country::where('deleted','=',0)->groupby('country_name')->get();
        if (is_null($rate)) {
            return Redirect::route('admin.rates.index');
        }
        return View::make("{$this->view_path}.edit", compact('rate','countries','rates_by_company'));
    }

    public function update($id)
    {   
        $input = Input::except('_token','company','rates_min','minutes_for_5');
        $minutes_for_5 = Input::get('minutes_for_5');
        $company=  Input::get('company');
        $rates_min = Input::get('rates_min');

        Validator::extend('validate_field', 'ratesvalidator@validate_field');
        $validation = ratesvalidator::create();
        if ($validation->passes()) {
            DB::transaction(function() use ($id,$input,$rates_min,$company,$minutes_for_5) {
                Rates::find($id)->update($input);
                DB::table('rates_by_company')->where('rates_id','=',$id)->update(array('deleted'=>1));
                if(!empty($rates_min))
                {
                foreach ( $rates_min as $key => $value) {
                    $check = DB::table('rates_by_company')
                                ->where('rates_id','=',$id)
                                ->where('company','=',$company[$key])->first();
                            
                                if(empty($check))
                                {
                                     $items[] = array(
                                            'rates_id'=>$id,
                                            'rates_min'=>$value,
                                            'minutes_for_5'=> $minutes_for_5[$key],
                                            'company'=>$company[$key],
                                        );
                                }
                                else{
                                    DB::table('rates_by_company')
                                ->where('company','=',$company[$key])
                                ->update(array('deleted'=>0));

                                }

                        if(!empty($items)){
                           DB::table('rates_by_company')->insert($items);
                        }

                } 
                }
            });
            if (Rates::find($id)->update($input)) {
                return Redirect::route('admin.rates.index')
                                ->with('success', 'Rate Updated successfully.');
            } else {
                return Redirect::route('admin.rates.index')
                                ->with('error', 'Rate updating error.');
            }
        }
        return Redirect::route('admin.rates.edit', $id)
                        ->withInput()
                        ->withErrors($validation->errors);
    }

    public function destroy($id) {
        if (Rates::find($id)->update(array("deleted" => "1"))) {
            return Redirect::route('admin.rates.index')
                            ->with('success', 'Rate deleted successfully.');
        } else {
            return Redirect::route('admin.rates.index')
                            ->with('error', 'Rate deleting error.');
        }
    }
}