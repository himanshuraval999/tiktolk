@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Contact us ::
@parent
@stop

{{-- Page content --}}
@section('content')

@include('frontend/notifications')
<div class="container">
 	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center">Get help From here.</h2>		

		</div>
	</div>
	 
 	<div class="row">
 		<div class="col-md-12">
			<div id="contact">
	          <img src="assets/images/contact.png" alt="info at tiktolk.com" class="img-responsive">
	          <div id="form">          
	            <form method="post" action="" id="contactform">
	            <input type="hidden" name="_token" value="{{ csrf_token() }}" />            	
	             	<div class="col-md-6">
		             	<ol id="formleft">
		                    <input type="text" name="contactname" id="contactname" value="" class="required">
		                    {{ $errors->first('contactname', '<span class="help-block">:message</span>') }}
		                    <input type="text" name="email" id="cntct_email" value="" class="required email">
		                    {{ $errors->first('email', '<span class="help-block">:message</span>') }}
		                    <textarea name="message" id="message" class="required" rows="" cols=""></textarea>
		                    {{ $errors->first('message', '<span class="help-block">:message</span>') }}
		                    <input type="submit" value="Send" name="submit" id="send">
			             </ol>	
	             	</div>
	            </form>
	          </div>
	          
	        </div>

 		</div>
 	</div>
	</div>
@stop
