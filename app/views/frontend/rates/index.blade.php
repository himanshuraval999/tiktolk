@extends('frontend/layouts/default')

@section('content')
	<div class="main">
    @include('frontend/notifications')
    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <h1 class="tag_line_h1">Let's Talk</h1>
        </div>
      </div>
      <div class="row shadow">
        <div class="col-sm-4 from_div">
          <h3 class="from">Call From</h3>
          <select style="width: 300px" id="from_cntry">

            @foreach($countrys as $key => $country)
                <option value="<?=strtolower($country['country_name']); ?>">{{$country['country_name']}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-sm-4 to_div">
          <h3 class="to">To</h3>
          <select style="width: 300px" id="to_cntry">
             @foreach($countrys as $key => $country)
                <option value="<?=strtolower($country['country_name']); ?>">{{$country['country_name']}}</option>
            @endforeach
          </select>
        </div>  
        <div class="col-sm-1 to_div">
        	<h3><label for=""></label></h3>
          <button class="btn btn-sm btn-success btn-go"><i class="fa fa-phone"></i> GO!</button>
        </div>
      </div>
      <div class="row results">
        <a class="close_div btn btn-sm btn-danger ">CLOSE</a>
        <div class="table">
          <table class="table table-stripped app_table " style="padding-left:10px">
	        	<thead>
              <tr>
  	        		<th>B/w lets Talks User</th>
  	        		<th>Landline User</th>
                <th>Mobile User</th>
  	        		<th>Unlimited (monthly)</th>
  	        	</tr>
            </thead>
            <tbody class="tbody_append">
                              
            </tbody>
	        </table>
	    </div>
      <br>
      <div class="allrate">
             
      </div>
      </div>
    </div>
  </div>
  <script>

   $(".btn-go").click(function(){
        var allrates_append="" ;
        $.ajax({
          type: "GET",
          dataType: 'json',
          url: "/ajaxRate",
          data: { from: $( "#from_cntry option:selected" ).text(),to: $( "#to_cntry option:selected" ).text() }
          ,success:function(data) {

            if(!jQuery.isEmptyObject( data.company ))
                {
                  var rates_min =  $.parseJSON(data.rates_min);
                  var minutes_for_5 =  $.parseJSON(data.minutes_for_5);
                  allrates_append += '<table class="table table-stripped app_table"><thead><tr><td>All rates for '+ data.to +'</td><td>Rate/Minute</td><td>Minutes for $5</td></tr></thead><tbody>';
                  $.each($.parseJSON(data.company) ,function(index,dd) {
                      allrates_append += '<tr>';
                      allrates_append += '<td>'+dd+'</td>';
                      allrates_append += '<td>'+rates_min[index]+'$</td>';
                      allrates_append += '<td>'+minutes_for_5[index]+'$</td>';
                      allrates_append += '</tr>';
                  });
                    allrates_append += '</tbody></table>';
          }
          else{
           allrates_append="No data found! For this country"; 
            $('.allrate').empty();
            $('.allrate').append(allrates_append);
          }
          var append_data = "";
          append_data = "<tr>";
          append_data += "<td>Free</td>";
          append_data += "<td>"+data.landline+"$</td>";
          append_data += "<td>"+data.mobile+"$</td>";
          append_data += "<td>"+data.unlimited+"$</td>";
          append_data += "</tr>";
          $('.allrate').empty();
          $('.allrate').append(allrates_append);
          $('.tbody_append').empty();
          $('.tbody_append').append(append_data);
        },
        error:function()
        {
          $('.tbody_append').empty();
          $('.allrate').empty();
          $('.tbody_append').append("No data found! Please contact our executive.");
        }
        });
    });
  </script>
@stop