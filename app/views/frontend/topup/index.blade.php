@extends('frontend/layouts/default')

@section('content')
<div class="main">
    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <h1 class="tag_line_h1">Topup </h1>
        </div>
      </div>
      <div class="row topup-shadow">
        <div class="col-md-3 text-right"><h3>Send A Top up</h3></div>
        <div class="col-md-4" style="margin-top:23px;margin-bottom:10px;">
        
        	<select style="width: 300px:height:20px;" id="to_cntry">
           @foreach($countrys as $key => $country)
              <option value="<?=strtolower($country['country_name']); ?>">{{$country['country_name']}}</option>
           @endforeach
          </select>
        
        </div>
        <div class="col-md-3" style="margin-top:23px;margin-bottom:10px;">
        	<input type="text" id="number" name="number"/>
        </div>  
        <div class="col-sm-2" style="margin-top:23px;margin-bottom:10px;">
        	<button class="btn btn-lg btn-success btn-go">Show option</button>
        </div>
      </div>
    </div>
  </div>

  <script>
   $("#to_cntry").on('change',function(){
   			// Fetch JSON from T2A API
			$.getJSON(
				"http://t2a.co/rest/?callback=?",
				{ 
				'method':'country_dial',
				'api_key':'sw9edtx-UZE7gM1t-5yotvjDzRItqiUw7adP5gmq1w_c', // better to use js_key obtained from javascript_key service
				'output': 'json',
				'text' : $(this).val()
				},
				// Function to parse resulting JSON
				function(result) {

				// Check for errors
				if(result.status == "ok") {

						$.each(result.country_dial_list, function() {

							console.log($("#number").val('+'+this.code+'-'));
						});
					} 
				}
			);
    });
  </script>
  @stop