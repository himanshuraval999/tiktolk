<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			TikTolk - Free International Call
			@show
		</title>
		<meta name="keywords" content="your, awesome, keywords, here" />
		<meta name="author" content="Jon Doe" />
		<meta name="description" content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei." />

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- CSS
		================================================== -->

		<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css')}}">
        
        <script src="{{ asset('assets/js/jquery.1.10.2.min.js')}}"></script>
        <script src="{{ asset('assets/js/bootstrap/bootstrap.js')}}"></script>
        <script src="{{ asset('assets/js/select2.js')}}"></script>

        <!--[if lte IE 9]>
          <script type="text/javascript" src="{{ asset('assets/js/html5.js')}}"></script>
          <script type="text/javascript" src="{{ asset('assets/js/html5shiv.js')}}"></script>
          <script type="text/javascript" src="{{ asset('assets/Respond/src/matchmedia.polyfill.js')}}"></script>
          <script type="text/javascript" src="{{ asset('assets/Respond/src/matchmedia.addListener.js')}}"></script>

          <script type="text/javascript" src="{{ asset('assets/Respond/src/respond.js')}}"></script>
          
          <style type="text/css"> 
            html,body {
               height:auto;
               width: 100%;
            }
            .row{
              width: 100%;
            }
          </style>
        <![endif]--> 
    
        <link rel="stylesheet" href="{{ asset('assets/css/custom-responsive.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/font-awesome/css/font-awesome.css')}}">

		<link href="{{ asset('assets/css/select2.css') }}" rel="stylesheet">
		
		<link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
        <script language="Javascript" src="http://www.codehelper.io/api/ips/?js"></script>
		
		
		<style>
			@section('styles')
			body {
				padding: 10px 0;
			}
			@show
		</style>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	</head>

	<body>
	<header>
		<div class="before-head"></div>
		<div class="container">
		<div class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
	    <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
			<a class="navbar-brand" href="{{ route('home') }}">
				<img src="{{ asset('assets/images/tiktok-2.jpg') }}"  class="img-responsive" alt="TilTok" height="100px" width="200px;">
			</a>
        </div>
	    <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav left-menu">
              <li {{ (Request::is('rates') ? 'class="active"' : '') }}><a href="{{ route('rates') }}">Rates</a></li>
              <li {{ (Request::is('topup') ? 'class="active"' : '') }}><a href="{{ route('topup') }}">TopUp</a></li>
              <li {{ (Request::is('contact-us') ? 'class="active"' : '') }}><a href="{{ URL::to('contact-us') }}">Support</a></li>
            </ul>
			<ul class="nav navbar-nav navbar-right right-btn">
           @if (Sentry::check())

           	<li class="dropdown{{ (Request::is('account*') ? ' active' : '') }}">
				<a class="btn-success dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="{{ route('account') }}">
					Welcome, {{ Sentry::getUser()->first_name }}
				</a>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					@if(Sentry::getUser()->hasAccess('admin'))
					<li {{ (Request::is('/') ? 'class="active"' : '') }}><a href="{{ route('admin') }}"><i class="icon-cog"></i> Administration</a></li>
					@endif
					<li{{ (Request::is('account/profile') ? ' class="active"' : '') }}><a href="{{ route('profile') }}"><i class="icon-user"></i> Your profile</a></li>
					<li><a href="{{ route('logout') }}"><i class="icon-off"></i> Logout</a></li>
				</ul>
			</li>
			@else



			    <li {{ (Request::is('auth/signup') ? 'class="active"' : '') }}>
		            <a class="btn-success homesignup" href="{{ route('signup') }}">
				    <i class="fa fa-user"></i>Sign Up</a> 
			    </li>
              	<li class="dropdown">
             		<a class="btn-success dropdown-toggle" href="#" data-toggle="dropdown" href="http://dotstrap.com/">
				    <i class="fa fa-user"></i> Login </a> 
				    <div class="dropdown-menu">
						<div class="login-container">
					            <div id="output"></div>
					            <div class="">User Login Form</div>
					            <br>
					            <div class="form-box">
					             	<form  method="post" action="{{ route('signin') }}">
					             		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					             		<input type="email" class="form-control" required="required" name="email" placeholder="Username">
					                    <input type="password" placeholder="password" class="form-control" name="password" require="required">
					                    <input class="btn btn-info btn-block login" type="submit" value="Login">
					           		</form>
					            </div>
					        </div>
					        
		            </div>
              	</li>
              	
			@endif
            </ul>
          </div><!--/.nav-collapse -->
	</div>
	</div>
	</div>
</header>


	<!-- Notifications -->
	<!-- Content -->
		
		
		@yield('content')
		<!-- Javascripts
		================================================== -->
	<script src="{{ asset('assets/js/jquery.backstretch.min.js')}}"></script>
	<script src="{{ asset('assets/js/vegas-bg.js')}}"></script>
	<script src="{{ asset('assets/js/custom.js')}}"></script>
	</body>
</html>
