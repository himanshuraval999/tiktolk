@extends('frontend/layouts/default')

{{-- Page content --}}
@section('content')
<div class="">
	<div class="row">
		<div class="col-md-2">
			<div class="navbar navbar-inverse navbar-twitch" role="navigation">
				<div class="container">
					<div class="">
						<ul class="nav navbar-nav">
							<li{{ Request::is('account/call-from-computer') ? ' class="active"' : '' }}><a href="">Call From Computer</a></li>
							<li{{ Request::is('account/account-overview') ? ' class="active"' : '' }}><a href="">Account Overview</a></li>
							<li{{ Request::is('account/loyaltiprogram') ? ' class="active"' : '' }}><a href="">Loyalty Program</a></li>
							<li{{ Request::is('account/registered-phone') ? ' class="active"' : '' }}><a href="">Registered Phones</a></li>
							<li{{ Request::is('account/send-gifts') ? ' class="active"' : '' }}><a href="">Send/Accept Gift</a></li>
							<li{{ Request::is('account/send-topup') ? ' class="active"' : '' }}><a href="">Send TopUp</a></li>
							<li{{ Request::is('account/billing-info') ? ' class="active"' : '' }}><a href="">Billing Information</a></li>
							<li{{ Request::is('account/access-number') ? ' class="active"' : '' }}><a href="">Access Numbers</a></li>
							<li{{ Request::is('account/settings') ? ' class="active"' : '' }}><a href="">Settings</a></li>
							<li{{ Request::is('account/profile') ? ' class="active"' : '' }}><a href="{{ URL::route('profile') }}">Profile</a></li>
							<li{{ Request::is('account/change-password') ? ' class="active"' : '' }}><a href="{{ URL::route('change-password') }}">Change Password</a></li>
							<li{{ Request::is('account/change-email') ? ' class="active"' : '' }}><a href="{{ URL::route('change-email') }}">Change Email</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>
		</div>
		<div class="col-md-10">
				@yield('account-content')
		</div>
@include('frontend/notifications')

@stop
