@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Account Sign up ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="container" >

	<div class="row form-wrap animated bounceIn" data-animation="bounceIn">
		<div class="title">
			<h3>Create an account</h3>
		</div>
		@include('frontend/notifications')
		<div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1">
			<div class="sns-signin">
				<a href="#" class="facebook"><i class="fa fa-facebook"></i><span>Sign in with Facebook </span></a>
				<a href="#" class="google-plus"><i class="fa fa-google-plus"></i><span>Sign in with Google+</span></a>
			</div>
			<div class="normal-signup">
				<div class="row">
				    <div class="col-md-12">
						<div id="form-olvidado">
							<h4 style="border-bottom: 1px solid #c5c5c5;">
								<i class="glyphicon glyphicon-user">
								</i>
								Already have an account? Log in
							</h4>
							<form accept-charset="UTF-8" role="form" id="login-form" method="post" action="{{ route('signin') }}">
		                        <input type="hidden" name="_token" value="{{csrf_token()}}">
		                        <fieldset>
								 	<div class="form-group">
									   Email : <input type="email" class="form-control" placeholder="Email" required="required" name="email">
									   Password : <input type="password" class="form-control" placeholder="Password" required="required" name="password">
									</div>
									<div class="form-group">
										<div class="row">
										   	<div class="col-md-5">
												<input type="submit" class="btn btn-block signup" value="Login" id="btn-olvidado">
											</div>
											<div class="col-md-5">
												<a class="btn btn-block signup" href="" id="olvidado">Reset password</a>
											</div>
										</div>		
									</div>
								</fieldset>
							</form>
						</div>
						<div style="display: none;" id="form-olvidado">
							<h4 class="">
						    	Forgot your password?
							</h4>
							<form accept-charset="UTF-8" role="form" id="login-recordar" action="{{ route('forgot-password') }}" method="post">
								<input type="hidden" name="_token" value="{{ csrf_token() }}" />
								<fieldset>
									<span class="help-block" style="color:white">
								    	Email address you use to log in to your account
								      	<br>
								      	We'll send you an email with instructions to Create a new password.
								    </span>
								    <div class="form-group">
										Email : <input type="email" class="form-control" placeholder="Email" required="required" name="email">
								    </div>
									<button type="submit" class="btn btn-primary btn-block" id="btn-olvidado">
								      Continue
								    </button>
								    <br>
								   	<div class="">
										<input type="submit" class="btn btn-block signup" value="Login" id="acceso">
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>	
		</div>
		<div class="col-md-2">
			<div class="horizontal-divider"></div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="normal-signup">
				<form method="post" action="{{ route('signup') }}" class="form-horizontal" >
		                        <input type="hidden" name="_token" value="{{csrf_token()}}">
				 	
				  <div class="form-group">
				   <div class="control-group{{ $errors->first('first_name', ' error') }}">
					<label class="control-label" for="first_name">First Name</label>
						<div class="controls">
							<input type="text"  name="first_name" class="form-control" placeholder="First Name" required="required"  id="first_name" value="{{ Input::old('first_name') }}" />
							{{ $errors->first('first_name', '<span class="help-block">:message</span>') }}
						</div>
					</div>
				  	<!-- Last Name -->
					<div class="control-group{{ $errors->first('last_name', ' error') }}">
						<label class="control-label" for="last_name">Last Name</label>
						<div class="controls">
							<input type="text" class="form-control" placeholder="Last Name" required="required"  name="last_name" id="last_name" value="{{ Input::old('last_name') }}" />
							{{ $errors->first('last_name', '<span class="help-block">:message</span>') }}
						</div>
					</div>

					<!-- Email -->
					<div class="control-group{{ $errors->first('email', ' error') }}">
						<label class="control-label" for="email">Email</label>
						<div class="controls">
							<input type="email" name="email" class="form-control" placeholder="Email" required="required" id="email"   value="{{ Input::old('email') }}" />
							{{ $errors->first('email', '<span class="help-block">:message</span>') }}
						</div>
					</div>

					<!-- Email Confirm -->
					<div class="control-group{{ $errors->first('email_confirm', ' error') }}">
						<label class="control-label" for="email_confirm">Confirm Email</label>
						<div class="controls">
							<input type="text" name="email_confirm" class="form-control" autocomplete="off" placeholder="Confirm Email" required="required" id="email"  id="email_confirm" value="{{ Input::old('email_confirm') }}" />
							{{ $errors->first('email_confirm', '<span class="help-block">:message</span>') }}
						</div>
					</div>

					<!-- Password -->
					<div class="control-group{{ $errors->first('password', ' error') }}">
						<label class="control-label" for="password">Password</label>
						<div class="controls">
							<input type="password" name="password" autocomplete="off" class="form-control" placeholder="Password" required="required" id="email"  id="password" value="" />
							{{ $errors->first('password', '<span class="help-block">:message</span>') }}
						</div>
					</div>

					<!-- Password Confirm -->
					<div class="control-group{{ $errors->first('password_confirm', ' error') }}">
						<label class="control-label" for="password_confirm">Confirm Password</label>
						<div class="controls">
							<input type="password" name="password_confirm" autocomplete="off" id="password_confirm" class="form-control" placeholder="Confirm password" required="required" id="email"  value="" />
							{{ $errors->first('password_confirm', '<span class="help-block">:message</span>') }}
						</div>
					</div>
					<!-- Password Confirm -->
					<div class="control-group{{ $errors->first('country_name', ' error') }}">
						<label class="control-label" for="country_name"> Country Name </label>
						<div class="controls">
							<select style="width: 300px" id="country_name" class="form-control" name="country">
						        @foreach($countries as $key => $country)
						            <option value="<?=strtolower($country['country_name']); ?>">{{$country['country_name']}}</option>
						        @endforeach
						    </select>
							{{ $errors->first('country_name', '<span class="help-block">:message</span>') }}
						</div>
					</div>
					<div class="control-group{{ $errors->first('phone_number', ' error') }}">
						<label class="control-label" for="phone_number"> Phone Number </label>
						<div class="controls">
				   			<input type="text" class="form-control" placeholder="Phone" name="phone_number" lass="form-control" placeholder="Confirm password" required="required" >
							{{ $errors->first('phone_number', '<span class="help-block">:message</span>') }}
						</div>
					</div>
				   
				  </div>
					<div class="row">
						<!-- <div class="col-md-6">
							<a href="#password_recovery" id="forgot_from_3">need help?</a>
						</div> -->
						<div class="col-md-5 col-md-offset-7">
							<button type="submit" class="btn btn-block signup">Sign Up</button>
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>
</div>

@if(isset($forgot_back))
   <script>
    $('div#form-olvidado').toggle('500');
   </script>
@endif

@stop
